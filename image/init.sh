#!/bin/bash
set -e

DRUPAL_SETTINGS_TEMPLATE="/drupal-config.d/settings.php.tpl"
DRUSH_SETTINGS_TEMPLATE="/drupal-config.d/drush.yml.tpl"
DRUPAL_SETTINGS_FILE="/var/www/web/sites/default/settings.php"
DRUSH_SETTINGS_FILE="/var/www/vendor/drush/drush/drush.yml"

main () {
    VERBOSE=0
    for i in "$@" ; do
        if [ "$i" == "-v" ] ; then
            set -x
            VERBOSE=1
            VERBOSE_ARG="$i"
            break
        fi
    done

    initialize
}

initialize() {
    # Restore most recent backup of Drupal web files when supplied
    if [ ! -d web/modules/contrib ]; then
        if ! restore-drupal.sh www --no-init "${VERBOSE_ARG}"; then
            echo "Unable to restore Drupal web files backup during initialization"
        else
            rm web/sites/default/settings.php
	    fi
    fi

    # Make sure file permission are safe
    restore_permissions

    # Initialize Drupal settings.php
    if [ ! -f "${DRUPAL_SETTINGS_FILE}" ]; then
        if [ -f "${DRUPAL_SETTINGS_TEMPLATE}" ]; then
            # Never debug output the following lines as they include credentials
            set +x
            echo -n 'Initializing Drupal with supplied configuration...'
            renderizer "${DRUPAL_SETTINGS_TEMPLATE}" \
                --Drupaldb="${DRUPALDB}" \
                --Drupaldb_user="${DRUPALDB_USER}" \
                --Drupaldb_pw="${DRUPALDB_PASSWORD}" \
                --Drupaldb_host="${DRUPALDB_HOST}" \
                --Drupal_hashsalt="${DRUPAL_HASHSALT}" \
                --Drupal_host_pattern="${DRUPAL_HOSTPATTERN}" \
                --Drupal_reverse_proxy_ips="${REVERSE_PROXY_IPS}" > "${DRUPAL_SETTINGS_FILE}"
            if [ "${REVERSE_PROXY_IPS}" = "" ]; then
                sed -i "/reverse_proxy_addresses/d" "${DRUPAL_SETTINGS_FILE}"
            fi
            reset_verbose
            # Setup Drush base URL
            if [ -f "${DRUSH_SETTINGS_TEMPLATE}" ]; then
                renderizer "${DRUSH_SETTINGS_TEMPLATE}" \
                    --Drupal_baseurl="${DRUPAL_BASEURL}" > "${DRUSH_SETTINGS_FILE}"
            fi
            echo "Done. "
        else
            echo 'FATAL: Failed to generate Drupal settings file. Template file "settings.php.tpl" not found in: /drupal-config.d/'
            exit 1
        fi
    fi
    chown -R root:"${FPM_USER}" "${DRUPAL_SETTINGS_FILE}" "${DRUSH_SETTINGS_FILE}"
    chmod 640 "${DRUPAL_SETTINGS_FILE}" "${DRUSH_SETTINGS_FILE}"

    # Never debug output the following lines as they include database credentials
    set +x
    # Wait for mysql db to be available
    until mysql -u "${DRUPALDB_USER}" "-p${DRUPALDB_PASSWORD}" -h "${DRUPALDB_HOST}"  -e 'exit' &> /dev/null; do
        echo "Mysql database is unavailable - sleeping"
        sleep 5
    done
    reset_verbose

    # Drupal not yet installed.
    if [ ! -d web/modules/contrib ]; then
        echo -n "Drupal not installed. Creating a minimal installation... "
        drush -y site-install standard --site-name="CLARIN Drupal"

        drush -y config-set system.performance js.preprocess 0
        drush -y config-set system.performance css.preprocess 0
        drush cache-rebuild
        restore_permissions 
        chmod 640 "${DRUPAL_SETTINGS_FILE}"
    else
        SITE_READONLY=0
        if ${DRUPAL_READONLY}; then
            SITE_READONLY=1
        fi
    	drush -y cset readonlymode.settings enabled ${SITE_READONLY}
    fi
    echo "Rebuilding cache... "
    drush cache-rebuild
    drush cron
    restore_permissions
    echo "Done."
}

restore_permissions () {
    echo -n "Restoring file permissions..."
    chown -R root:"${FPM_USER}" web
    find web -type d -exec chmod 750 '{}' \;
    find web -type f -exec chmod 640 '{}' \;
    if [ "$(ls -A web/sites/*/files*)" ]; then
        for d in web/sites/*/files*
        do
            find "${d}" -type d -exec chmod 770 '{}' \;
            find "${d}" -type f -exec chmod 660 '{}' \;
        done
    fi
    echo "Done. "
}

reset_verbose () {
    if [ ${VERBOSE} -eq 1 ]; then
        echo -n "WARNING: some lines were omitted from the debug output to protect credentials."
        set -x
    fi
}

main "$@"; exit
