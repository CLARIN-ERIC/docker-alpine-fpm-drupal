#!/bin/bash
set -e

main() {
    VERBOSE=0
    INIT=1
    HELP=0
    ERROR=0
    #
    # Process script arguments and parse all optional flags
    #
    while [[ $# -gt 0 ]]
    do
    key="$1"
    case $key in
        -v|--verbose)
            set -x
            VERBOSE=1
            VERBOSE_ARG="${key}"
            ;;
        -h|--help)
            HELP=1
            ;;
        -i|--no-init)
            INIT=0
            ;;
        *)
            ARG_REMAINDER_ARRAY+=("${key}")
            ;;
    esac
    shift # past argument or value
    done

    BACKUPS_DIR=${BACKUPS_DIR:-/backups}
    DB_BACKUPS_DIR=${DB_BACKUPS_DIR:-${BACKUPS_DIR}/database}
    HTTP_BACKUPS_DIR=${HTTP_BACKUPS_DIR:-${BACKUPS_DIR}/htdocs}

    # By default restore all
    if [ -z "${ARG_REMAINDER_ARRAY[0]}" ] && [ "$HELP" -eq 0 ]; then
        ARG_REMAINDER_ARRAY[0]="all"
    fi

    case ${ARG_REMAINDER_ARRAY[0]} in
        db)
            restore_db "${ARG_REMAINDER_ARRAY[1]}"
            set_read_only_state
            exit 0
            ;;
        www)
            restore_webfiles "${ARG_REMAINDER_ARRAY[1]}"
            set_read_only_state
            exit 0
            ;;
        all)
            restore_all "${ARG_REMAINDER_ARRAY[1]}"
            set_read_only_state
            exit 0
            ;;
        *)
            echo "Invalid parameter: ${1}"
            HELP=1
            ERROR=1
            ;;
    esac

    if [ "${HELP}" -eq 1 ]; then
        echo ""
        echo "Usage: ${0#./} [-h|-v|--no-init] [db|www|all] [backup_timestamp]"
        echo ""
        echo "  Optional flags:"
        echo "    -v, --verbose                     Run in verbose mode"
        echo "    -h, --help                        Show help"
        echo "    -i, --no-init                     Skip container initialization. (for container internal use)"
        echo ""
        echo "  Commands:"
        echo "    db [backup_timestamp]             Restore latest database backup available, or the backup file"
        echo "                                       containing [backup_timestamp] if supplied."
        echo "    www [backup_timestamp]            Restore latest web files backup available, or the backup file"
        echo "                                       containing [backup_timestamp] if supplied."
        echo "    all [backup_timestamp]            Restore latest database and web files backups, or the backup files"
        echo "                                       containing [backup_timestamp] if supplied."
        echo ""
        echo "  Placeholder [backup_timestamp] in file name template:"
        echo "             backup_drupal_<db|www]>_[backup_timestamp].<sql|tar>.gz"
        echo ""

        if [ "${ERROR}" -eq 0 ]; then
            exit 0
        else
            exit 1
        fi
    fi
}

restore_all () {
    STARTT=$(date +%s)
    restore_db "$1"
    restore_webfiles "$1"
    ENDT=$(date +%s)
    echo "Full backup restored in $((ENDT-STARTT)) seconds."
}

restore_db () {
    TIMESTAMP="latest"
    if [ -z "$1" ]; then
        # shellcheck disable=SC2012
        BACKUPFILE=$(realpath "${DB_BACKUPS_DIR}"/"$(basename "$(ls -at "${DB_BACKUPS_DIR}"/*drupal*.sql.gz | head -1)")")
    else
        TIMESTAMP="$1"
        BACKUPFILE=$(find "${BACKUPS_DIR}" -name "backup_drupal_db_*${TIMESTAMP}.sql.gz" 2> /dev/null| head -1)
    fi
    if [ -z "${BACKUPFILE}" ] || [ ! -f  "${BACKUPFILE}" ]; then
        echo "Backup file: \"${BACKUPFILE}\" with timestamp: \"${TIMESTAMP}\" not found."
        exit 1
    fi
    echo "Restoring Drupal database backup: ${BACKUPFILE} ... "
    STARTT_PARTIAL=$(date +%s)
    # Never debug output the following 2 lines as they include database credentials
    set +x
    mysql_options=("-h${DRUPALDB_HOST}" "-u${DRUPALDB_USER}" "-p${DRUPALDB_PASSWORD}")
    (echo "DROP DATABASE IF EXISTS ${DRUPALDB};CREATE DATABASE ${DRUPALDB};" | mysql "${mysql_options[@]}") \
    && (gunzip -c "${BACKUPFILE}" | mysql "${mysql_options[@]}" "${DRUPALDB}")
    if [ ${VERBOSE} -eq 1 ]; then
        echo -n "WARNING: mysql restore lines were omitted from debug output to protect the database credentials."
        set -x
    fi
    ENDT=$(date +%s)
    echo "Drupal database successfully restored in $((ENDT-STARTT_PARTIAL)) seconds."
}

restore_webfiles () {
    TIMESTAMP="latest"
    if [ -z "$1" ]; then
        # shellcheck disable=SC2012
        BACKUPFILE=$(realpath "${HTTP_BACKUPS_DIR}"/"$(basename "$(ls -at "${HTTP_BACKUPS_DIR}"/*drupal*.tar.gz | head -1)")")
    else
        TIMESTAMP="$1"
        BACKUPFILE=$(find "${BACKUPS_DIR}" -name "backup_drupal_www_*${TIMESTAMP}.tar.gz" 2> /dev/null| head -1)
    fi
    if [ -z "${BACKUPFILE}" ] || [ ! -f  "${BACKUPFILE}" ]; then
        echo "Backup file: \"${BACKUPFILE}\" with timestamp: \"${TIMESTAMP}\" not found."
        exit 1
    fi
    if [ "$(ls -A "${WEBSITE_HOME}/web/")" ]; then
        (shopt -s dotglob; rm -rf "${WEBSITE_HOME}/web/*")
    fi
    # Restore Drupal web files when supplied
    echo "Restoring Drupal web files backup: ${BACKUPFILE} ... "
    STARTT_PARTIAL=$(date +%s)
    tar zxf"$([[ ${VERBOSE} -eq 1 ]] && echo -n 'v' || echo -n '')" "${BACKUPFILE}" -C .
    ENDT=$(date +%s)
    echo "Drupal web files successfully restored in $((ENDT-STARTT_PARTIAL)) seconds."
    
    if [ ${INIT} -eq 1 ]; then
        echo "Reinitializing local configuration ..."
        rm web/sites/default/settings.php
        rm vendor/drush/drush/drush.yml
        restart_php_fpm
        # shellcheck source=/dev/null
        source /init/docker-alpine-fpm-drupal-init.sh "${VERBOSE_ARG}"
    fi
}

set_read_only_state () {
    SITE_READONLY=0
    if ${DRUPAL_READONLY}; then
        SITE_READONLY=1
    fi
    (cd "${WEBSITE_HOME}/web/" && \
    if [ -f sites/default/settings.php ]; then 
        drush -y cset readonlymode.settings enabled ${SITE_READONLY}; 
    fi)
}

restart_php_fpm () {
    supervisorctl -u sysops -p thepassword restart php-fpm
}

main "$@"; exit
