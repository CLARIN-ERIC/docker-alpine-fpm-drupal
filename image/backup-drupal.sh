#!/bin/bash
set -e

main() {
    VERBOSE=0

    for i in "$@" ; do
        if [ "$i" == "-v" ] ; then
            set -x
            VERBOSE=1
        fi
    done

    if [ -z "${BACKUPS_TIMESTAMP}" ]; then
	   echo "No timestamp provided by the control script"
	   exit 1
    fi

    BACKUPS_DIR=${BACKUPS_DIR:-/backups}
    DB_BACKUPS_DIR=${DB_BACKUPS_DIR:-${BACKUPS_DIR}/database}
    HTTP_BACKUPS_DIR=${HTTP_BACKUPS_DIR:-${BACKUPS_DIR}/htdocs}

    STARTT=$(date +%s)
    backup
    ENDT=$(date +%s)
    echo "Full backup created in $((ENDT-STARTT)) seconds."
    exit 0
}

backup() {
    echo "Backing up Drupal database and web files... "

    drush -y cset readonlymode.settings enabled 1
    if [ ! -d "${DB_BACKUPS_DIR}/" ]; then
        mkdir -p "${DB_BACKUPS_DIR}/"
    fi
    if [ ! -d "${HTTP_BACKUPS_DIR}/" ]; then
        mkdir -p "${HTTP_BACKUPS_DIR}/"
    fi
    echo -n "Backing up Drupal database... "
    STARTT_PARTIAL=$(date +%s)
    # Never debug output the following 2 lines as they include database credentials
    set +x
    mysql_options=("--skip-lock-tables" "--single-transaction" "--events" "-h${DRUPALDB_HOST}" "-u${DRUPALDB_USER}" "-p${DRUPALDB_PASSWORD}" "${DRUPALDB}")
    mysqldump "${mysql_options[@]}" | gzip -9 > "${DB_BACKUPS_DIR}/backup_drupal_db_${BACKUPS_TIMESTAMP}.sql.gz"
    if [ ${VERBOSE} -eq 1 ]; then
        echo -n "WARNING: mysqldump lines were omitted from debug output to protect the database credentials."
        set -x
    fi
    ln -fs "backup_drupal_db_${BACKUPS_TIMESTAMP}.sql.gz" "${DB_BACKUPS_DIR}/backup_drupal_db.sql.gz"
    ENDT=$(date +%s)
    echo "Drupal database backup successfully saved to: ${DB_BACKUPS_DIR}/backup_drupal_db_${BACKUPS_TIMESTAMP}.sql.gz in $((ENDT-STARTT_PARTIAL)) seconds."

    echo -n "Backing up Drupal web files... "
    STARTT_PARTIAL=$(date +%s)
    cd "${WEBSITE_HOME}"
    tar czf"$([[ ${VERBOSE} -eq 1 ]] && echo -n 'v' || echo -n '')" "${HTTP_BACKUPS_DIR}/backup_drupal_www_${BACKUPS_TIMESTAMP}.tar.gz" . 
    ln -fs "backup_drupal_www_${BACKUPS_TIMESTAMP}.tar.gz" "${HTTP_BACKUPS_DIR}/backup_drupal_www.tar.gz"
    ENDT=$(date +%s)
    echo "Drupal directory backup successfully saved to: ${HTTP_BACKUPS_DIR}/backup_drupal_www_${BACKUPS_TIMESTAMP}.tar.gz in $((ENDT-STARTT_PARTIAL)) seconds."

    SITE_READONLY=0
    if ${DRUPAL_READONLY}; then
        SITE_READONLY=1
    fi
    drush -y cset readonlymode.settings enabled "${SITE_READONLY}"
}

main "$@";
