<?php

$databases['default']['default'] = array (
  'database' => '{{ .Drupaldb }}',
  'username' => '{{ .Drupaldb_user }}',
  'password' => '{{ .Drupaldb_pw }}',
  'prefix' => false,
  'host' => '{{ .Drupaldb_host }}',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
  'charset' => 'utf8mb4',
  'collation' => 'utf8mb4_general_ci',
);

$settings['update_free_access'] = FALSE;
$settings['hash_salt'] = "{{ .Drupal_hashsalt }}";

$config['system.performance']['fast_404']['exclude_paths'] = '/\/(?:styles)|(?:system\/files)\//';
$config['system.performance']['fast_404']['paths'] = '/\.(?:txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';
$config['system.performance']['fast_404']['html'] = '<!DOCTYPE html><html><head><title>404 Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';
$config['automated_cron.settings']['interval'] = 0;

$settings['allow_authorize_operations'] = FALSE;

$settings['trusted_host_patterns'] = array(
    '{{ .Drupal_host_pattern }}',
);
$settings['reverse_proxy'] = TRUE;
$settings['reverse_proxy_trusted_headers'] = \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_FOR |
\Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_PROTO | \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_PORT; 
$settings['reverse_proxy_addresses'] = array({{ .Drupal_reverse_proxy_ips }});

/**                                                                   
 * Configuration sync directory                                       
 */                                                                   
$settings["config_sync_directory"] = '../config/sync';                
                                                                      
$settings['file_public_path'] = 'sites/default/files';                
$settings['file_private_path'] = '/var/www/private';  
                                                      
$settings['php_storage']['twig']['directory'] = 'sites/default/files';

$settings['state_cache'] = TRUE;
